# Compass Config
preferred_syntax = :scss

sass_dir = "./src/app/scss"
images_dir = "./src/app/images/sprites"
generated_images_dir = "src/app/images"
css_dir = "src/app/css"
http_path = "/"
http_stylesheets_path = "css"
http_generated_images_path = "images"

relative_assets = true
line_comments = true
#sourcemap = true

# output_style = :expanded or :nested or :compact or :compressed
output_style = :compressed


# disable asset cache buster
asset_cache_buster do |http_path, real_path|
  nil
end