'use strict';
var path = require('path'),
	modRewrite = require('connect-modrewrite'),
	wd = path.join(__dirname, '..'),
	config = {
		server: {
			name: 'localhost',
			port: 3000,
			root: path.join(wd, 'build')
		},
		build: {
			dst: path.join(wd, 'build')
		},
		html: {
			watch: [
				path.join(wd, 'src', 'app', '*.html'),
				path.join(wd, 'src', 'app', 'views', '**', '*.html')
			],
			actions: {
				gzip: {
					enabled: true,
					options: {
						append: true, //append .gz to files or not
						gzipOptions: {level: 9}
					}
				},
				min: {
					enabled: true
				}
			},
			views: {
				src: path.join(wd, 'src', 'app', 'views', '**', '*.html'),
				dst: path.join(wd, 'build', 'views')
			},
			index: {
				src: path.join(wd, 'src', 'app', '*.html'),
				dst: path.join(wd, 'build')
			},
			all: {
				src: path.join(wd, 'src', 'app', '**', '*.html'),
				dst: path.join(wd, 'build')
			}

		},

		scripts: {
			watch: [
				path.join(wd, 'src', 'app', 'js', '**', '*.js')
			],
			actions: {
				lint: {
					enabled: true,
					src: [
						path.join(wd, 'src', 'app', 'js', '**', '*.js'),
						'!' + path.join(wd, 'src', 'app', 'js', 'vendor', '**', '*.js')
					]
				},
				browserify: {
					entries: [path.join(wd, 'src', 'app', 'js', 'main.js')],
					bundleName: 'main.js'
				},
				gzip: {
					enabled: true,
					options: {
						append: true, //append .gz to files or not
						gzipOptions: {level: 9}
					}
				},
				min: {
					// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
					enabled: true,
					options: {
						mangle: true,
						compress: {
							sequences: true,  // join consecutive statemets with the “comma operator”
							properties: true,  // optimize property access: a["foo"] → a.foo
							dead_code: true,  // discard unreachable code
							drop_debugger: true,  // discard “debugger” statements
							drop_console: true,  // discard “console.*” statements
							unsafe: false, // some unsafe optimizations (see below)
							conditionals: true,  // optimize if-s and conditional expressions
							comparisons: true,  // optimize comparisons
							evaluate: true,  // evaluate constant expressions
							booleans: true,  // optimize boolean expressions
							loops: true,  // optimize loops
							unused: false,  // drop unused variables/functions
							hoist_funs: true,  // hoist function declarations
							hoist_vars: false, // hoist variable declarations
							if_return: true,  // optimize if-s followed by return/continue
							join_vars: true,  // join var declarations
							cascade: false,  // try to cascade `right` into `left` in sequences
							side_effects: false,  // drop side-effect-free statements
							warnings: false // warn about potentially dangerous optimizations/code
						}
					}
					// jscs:enable requireCamelCaseOrUpperCaseIdentifiers
				}
			},
			src: [
				path.join(wd, 'src', 'app', 'js', '**', '*.js'),
				'!' + path.join(wd, 'src', 'app', 'js', 'lib', '**', '*.js')
			],
			dst: path.join(wd, 'build', 'js')
		},
		jslibs: {
			watch: [
				path.join(wd, 'src', 'app', 'js', 'lib', '**', '*.js')
			],
			actions: {
				gzip: {
					enabled: true,
					options: {
						append: true, //append .gz to files or not
						gzipOptions: {level: 9}
					}
				}
			},
			src: [
				path.join(wd, 'src', 'app', 'js', 'lib', '**', '*.js'),
				'!' + path.join(wd, 'src', 'app', 'js', 'lib', '**', 'src', '**', '*.js')
			],
			dst: path.join(wd, 'build', 'js', 'lib')
		},
		css: {
			actions: {
				min: {
					enabled: true,
					options: {
						cache: true,
						rebase: false
					}
				},
				gzip: {
					enabled: true,
					options: {
						append: true, //append .gz to files or not
						gzipOptions: {level: 9}
					}
				}
			},
			watch: [
				path.join(wd, 'src', 'app', 'css', '**', '*'),
				path.join(wd, 'src', 'app', 'scss', '**', '*')
			],
			src: [
				path.join(wd, 'src', 'app', 'css', '**', '*.css')
			],
			dst: path.join(wd, 'build', 'css')
		},

		scss: {
			actions: {
				compile: {
					enabled: true,
					options: {
						// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
						config_file: path.join(wd, 'config.rb'),
						// jscs:enable requireCamelCaseOrUpperCaseIdentifiers
						css: 'src/app/css',
						sass: 'src/app/scss',
						require: ['susy', 'normalize-scss', 'sass-media_query_combiner', 'autoprefixer-rails']
					}
				}
			},
			src: [
				path.join(wd, 'src', 'app', 'scss', '**', '*')
			],
			dst: path.join(wd, 'src', 'app', 'css')
		},

		images: {
			watch: [
				path.join(wd, 'src', 'app', 'images', '**', '*.*'),
				'!' + path.join(wd, 'src', 'app', 'images', 'sprites', '**', '*')
			],
			actions: {
				min: {
					enabled: true,
					options: {
						progressive: true,
						interlaced: true,
						optimizationLevel: 5
						/*use: [pngquant({quality: '80-90', speed: 1})]*/
					}
				}
			},
			src: [
				path.join(wd, 'src', 'app', 'images', '**', '*.*'),
				'!' + path.join(wd, 'src', 'app', 'images', 'sprites', '**', '*')
			],
			dst: path.join(wd, 'build', 'images')
		},

		preprocess: {
			enabled: true
		}
	};

config.browserSync = {
	options: {
		port: config.server.port,
		ghostMode: {
			clicks: true,
			forms: true,
			scroll: false
		},
		server: {
			baseDir: config.server.root,
			middleware: [
				modRewrite([
					'!\\.[\\w?=&@]+$ /index.html [L]'
				])
			]
		}
	}
};
module.exports = config;
