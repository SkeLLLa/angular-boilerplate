/**
 * Created by m03geek on 21.01.15.
 */

'use strict';

var browserSync = require('browser-sync'),
	reload = browserSync.reload,
	browserify = require('browserify'),
	ngAnnotate = require('browserify-ngannotate'),
	watchify = require('watchify'),
	source = require('vinyl-source-stream'),
	buffer = require('vinyl-buffer'),
	runSequence = require('run-sequence'),
	gulp = require('gulp'),
	$ = require('gulp-load-plugins')(),
	pJson = require('./package.json'),
	config = require('./gulp/config'),
	del = require('del'),
	build = function (callback) {
		runSequence('clean', 'compass', ['build:css', 'build:js', 'build:images', 'build:html', 'build:jslibs'], callback);
	};

gulp.task('watch:html', function () {
	return gulp.src(config.html.all.src)
		.pipe($.replace('@@version@@', pJson.version))
		.pipe($.if(config.preprocess.enabled, $.preprocess(config.preprocess.options)))
		.pipe($.if(!config.html.actions.min.enabled, gulp.dest(config.html.all.dst)))
		.pipe($.if(config.html.actions.min.enabled, $.minifyHtml()))
		.pipe($.if(config.html.actions.min.enabled, gulp.dest(config.html.all.dst)))
		.pipe(browserSync.reload({stream: true}));
});

gulp.task('build:html', function () {
	return gulp.src(config.html.all.src)
		.pipe($.replace('@@version@@', pJson.version))
		.pipe($.if(config.preprocess.enabled, $.preprocess(config.preprocess.options)))
		.pipe($.if(!config.html.actions.min.enabled, gulp.dest(config.html.all.dst)))
		.pipe($.size({title: 'html:'}))
		.pipe($.if(config.html.actions.min.enabled, $.minifyHtml()))
		.pipe($.if(config.html.actions.min.enabled, gulp.dest(config.html.all.dst)))
		.pipe($.size({title: 'html:minified'}))
		.pipe($.if(config.html.actions.gzip.enabled, $.gzip(config.html.actions.gzip.options)))
		.pipe($.if(config.html.actions.gzip.enabled, gulp.dest(config.html.all.dst)))
		.pipe($.size({title: 'html:gzipped'}));
});

gulp.task('watch:js', function () {
	var b = browserify({
			entries: config.scripts.actions.browserify.entries,
			cache: {},
			packageCache: {},
			fullPaths: true
		}, watchify.args),
		bundler = watchify(b);
	bundler.on('update', function () {
		bundler.transform(ngAnnotate)
			.transform('brfs')
			.bundle()
			.pipe(source('main.js')) // gives streaming vinyl file object
			.pipe(buffer())
			.pipe($.replace('@@version@@', pJson.version))
			.pipe(gulp.dest(config.scripts.dst))
			.pipe($.if(!config.scripts.actions.min.enabled, gulp.dest(config.scripts.dst)))
			.pipe($.if(config.scripts.actions.min.enabled, $.streamify($.uglify(config.scripts.actions.min.options))))
			.pipe($.if(config.scripts.actions.min.enabled, gulp.dest(config.scripts.dst)))
			.pipe(browserSync.reload({stream: true, once: true}));
	});

	return b.transform(ngAnnotate)
		.transform('brfs')
		.bundle()
		.pipe(source('main.js')) // gives streaming vinyl file object
		.pipe(buffer())
		.pipe($.replace('@@version@@', pJson.version))
		.pipe(gulp.dest(config.scripts.dst))
		.pipe($.if(!config.scripts.actions.min.enabled, gulp.dest(config.scripts.dst)))
		.pipe($.if(config.scripts.actions.min.enabled, $.streamify($.uglify(config.scripts.actions.min.options))))
		.pipe($.if(config.scripts.actions.min.enabled, gulp.dest(config.scripts.dst)))
		.pipe(browserSync.reload({stream: true, once: true}));

});

gulp.task('lint:js', function () {
	return gulp.src(config.scripts.src)
		.pipe($.jscs());
});

gulp.task('build:js', ['lint:js'], function () {
	return browserify({
		entries: config.scripts.actions.browserify.entries,
		cache: {},
		packageCache: {},
		fullPaths: true
	}, watchify.args)
		.transform(ngAnnotate)
		.transform('brfs')
		.bundle()
		.pipe(source('main.js'))
		.pipe(buffer())
		.pipe($.replace('@@version@@', pJson.version))
		.pipe($.if(!config.scripts.actions.min.enabled, gulp.dest(config.scripts.dst)))
		.pipe($.size({title: 'js'}))
		.pipe($.if(config.scripts.actions.min.enabled, $.streamify($.uglify(config.scripts.actions.min.options))))
		.pipe($.if(config.scripts.actions.min.enabled, gulp.dest(config.scripts.dst)))
		.pipe($.size({title: 'js:minified'}))
		.pipe($.if(config.scripts.actions.gzip.enabled, $.gzip(config.scripts.actions.gzip.options)))
		.pipe($.if(config.scripts.actions.gzip.enabled, gulp.dest(config.scripts.dst)))
		.pipe($.size({title: 'js:gzipped'}))
});

gulp.task('build:jslibs', function () {
	return gulp.src(config.jslibs.src)
		.pipe($.size({title: 'jslibs'}))
		.pipe($.flatten())
		.pipe(gulp.dest(config.jslibs.dst))
		.pipe($.if(config.jslibs.actions.gzip.enabled, $.gzip(config.jslibs.actions.gzip.options)))
		.pipe($.if(config.jslibs.actions.gzip.enabled, gulp.dest(config.jslibs.dst)))
		.pipe($.size({title: 'jslibs:gzipped'}))
});

gulp.task('watch:jslibs', function () {
	return gulp.src(config.jslibs.src)
		.pipe($.flatten())
		.pipe($.changed(config.jslibs.dst))
		.pipe(gulp.dest(config.jslibs.dst))
});

gulp.task('build:images', function () {
	return gulp.src(config.images.src)
		.pipe($.size({title: 'images'}))
		.pipe($.if(!config.images.actions.min.enabled, gulp.dest(config.images.dst)))
		.pipe($.if(config.images.actions.min.enabled, $.imagemin(config.images.actions.min.options)))
		.pipe($.if(config.images.actions.min.enabled, gulp.dest(config.images.dst)))
		.pipe($.size({title: 'images:optimized'}))
});

gulp.task('watch:images', function () {
	return gulp.src(config.images.src)
		.pipe($.changed(config.images.dst))
		.pipe(gulp.dest(config.images.dst))
});

gulp.task('compass', function () {
	return gulp.src(config.scss.src)
		.pipe($.compass(config.scss.actions.compile.options));
});

gulp.task('build:css', function () {
	return gulp.src(config.css.src)
		.pipe($.size({title: 'css'}))
		.pipe($.concat('style.css'))
		.pipe($.if(!config.css.actions.min.enabled, gulp.dest(config.css.dst)))
		.pipe($.if(config.css.actions.min.enabled, $.minifyCss(config.css.actions.min.options)))
		.pipe($.if(config.css.actions.min.enabled, gulp.dest(config.css.dst)))
		.pipe($.if(config.css.actions.gzip.enabled, $.gzip(config.css.actions.gzip.options)))
		.pipe($.if(config.css.actions.gzip.enabled, gulp.dest(config.css.dst)))
		.pipe($.size({title: 'css:gzipped'}))
});

gulp.task('watch:css', ['compass'], function () {
	return gulp.src(config.css.src)
		.pipe($.changed(config.css.dst))
		.pipe(gulp.dest(config.css.dst))
		.pipe(browserSync.reload({stream: true}));
});

gulp.task('browserSync', function () {
	browserSync(config.browserSync.options);
});

gulp.task('dev:watch', ['browserSync', 'watch:html', 'lint:js', 'watch:js', 'watch:images', 'watch:css', 'watch:jslibs'], function () {
	config.preprocess.options = {
		context: {
			env: 'dev',
			debug: true
		}
	};
	gulp.watch(config.html.watch, ['watch:html']);
	gulp.watch(config.scripts.watch, ['lint:js', 'watch:js']);
	gulp.watch(config.images.watch, ['watch:images']);
	gulp.watch(config.css.watch, ['watch:css']);
	gulp.watch(config.jslibs.watch, ['watch:jslibs']);
});

gulp.task('clean', function (cb) {
	del(config.build.dst, {force: true}, cb);
});

gulp.task('dev:build', function (cb) {
	config.preprocess.options = {
		context: {
			env: 'dev'
		}
	};
	build(cb);
});

gulp.task('prod:build', function (cb) {
	config.preprocess.options = {
		context: {
			env: 'prod'
		}
	};
	build(cb);
});
